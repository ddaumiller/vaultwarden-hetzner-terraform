terraform {
  required_providers {
    hcloud = {
      source  = "hetznercloud/hcloud"
      version = ">= 0.13"
    }
    template = { version = "~> 2.2" }
    # ssh = {
    #   source = "loafoe/ssh"
    #   version = "~> 2.6"
    # }
  }
}

# Set the variable value in *.tfvars file
# or using the -var="hcloud_token=..." CLI option
variable "HCLOUDTOKEN" {
  type = string
}

# Configure the Hetzner Cloud Provider
provider "hcloud" {
  token = var.HCLOUDTOKEN
}

# resource "hcloud_firewall" "vaultwardenFW" {
#   name = "vaultwarden firewall"
#   rule {
#     source_ips = ["0.0.0.0/0", "::/0"]
#     direction  = "in"
#     protocol   = "tcp"
#     port       = 80
#   }
#   rule {
#     source_ips = ["0.0.0.0/0", "::/0"]
#     direction  = "in"
#     protocol   = "tcp"
#     port       = 443
#   }
#   rule {
#     source_ips = ["0.0.0.0/0", "::/0"]
#     direction  = "in"
#     protocol   = "tcp"
#     port       = 2815
#   }
#   apply_to {
#     server = hcloud_server.containerhost.id
#   }
# }

resource "hcloud_primary_ip" "main" {
  name          = "vaultwarden_ip6"
  type          = "ipv6"
  auto_delete   = false
  assignee_type = "server"
  datacenter    = "fsn1-dc14"
  delete_protection = true

  # lifecycle {
  #   prevent_destroy = true
  # }
}

resource "hcloud_ssh_key" "default" {
  name       = "cloud-init"
  public_key = file("./tf-cloud-init.pub")
}
# Create a server
resource "hcloud_server" "containerhost" {
  name        = "containerhost"
  image       = "ubuntu-22.04"
  server_type = "cax11"
  datacenter  = "fsn1-dc14"
  ssh_keys    = [hcloud_ssh_key.default.id]
  user_data   = data.template_file.user_data.rendered

  # provisioner "file" {
  #   connection {
  #     type        = "ssh"
  #     user        = "terraform"
  #     port = 2815
  #     private_key = file("${path.module}/tf-cloud-init")
  #     host        = hcloud_primary_ip.main.ip_address
  #   }
  #   destination = "/opt/vaultwarden/"
  #   source = "vaultwarden_compose/"
  # }
  public_net {
    ipv6         = hcloud_primary_ip.main.id
    ipv4_enabled = false
  }
}

output "ip_address" {
  description = "public IP address (add 1)"
  value = hcloud_primary_ip.main.ip_address
}
# resource "ssh_resource" "fail2ban" {
#   host         = hcloud_server.containerhost.id
#   # bastion_host = "jumpgate.remote-host.com"
#   user         = "terraform"
#   agent        = true

#   file {
#     content     = file("${path.module}/fail2ban/*")
#     destination = "/etc/fail2ban/"
#     permissions = "0700"
#   }

#   timeout = "3m"
# }

# output "result" {
#   value = try(jsondecode(ssh_resource.example.result), {})
# }

data "template_file" "user_data" {
  template = file("./cloud-init.yaml")
}


# Create a volume
resource "hcloud_volume" "storage" {
  name      = "my-volume"
  size      = 50
  server_id = hcloud_server.containerhost.id
  automount = true
  format    = "ext4"
}