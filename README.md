store an API token for hetzner cloud in your secret store like this:

    $ secret-tool store server console.hetzner.cloud --label 'hetzner api token'
    Password: *****

vscode will try to retrieve that token when starting the devcontainer.

create a new ssh key to be used in the server:

    $ ssh-keygen -t ed25519 -C "terry@containerhost" -f tf-cloud-init

TODO:

 - autogenerate SSH key as in https://dev.to/admantium/terraform-workshop-manage-hetzner-cloud-servers-3ih1
 - autodeploy docker payload
 - set up vaultwarden
 - set up data backup?